# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-18 00:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot_app', '0019_auto_20180117_2252'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutoPosting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(blank=True, max_length=255, null=True, verbose_name='Текст')),
                ('everyday', models.BooleanField(default=False, verbose_name='Каждый день')),
                ('in_one_day', models.BooleanField(default=False, verbose_name='Через день')),
                ('monday', models.BooleanField(default=False, verbose_name='Понедельник')),
                ('tuesday', models.BooleanField(default=False, verbose_name='Вторник')),
                ('wednesday', models.BooleanField(default=False, verbose_name='Среда')),
                ('thursday', models.BooleanField(default=False, verbose_name='Четверг')),
                ('friday', models.BooleanField(default=False, verbose_name='Пятница')),
                ('saturday', models.BooleanField(default=False, verbose_name='Суббота')),
                ('sunday', models.BooleanField(default=False, verbose_name='Воскресенье')),
                ('source', models.CharField(choices=[('my_post', 'Мой пост'), ('twitter', 'twitter'), ('vk', 'vk'), ('youtube', 'youtube'), ('facebook', 'facebook'), ('instagram', 'instagram'), ('rss', 'rss')], default='my_post', max_length=255, verbose_name='Тип поста')),
                ('published_time', models.CharField(blank=True, max_length=255, null=True, verbose_name='время публикации')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='auto_postings', to='bot_app.Bot')),
            ],
            options={
                'verbose_name': 'Авто постинг',
                'verbose_name_plural': 'Автопостинги',
            },
        ),
    ]
