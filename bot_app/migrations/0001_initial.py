# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-10 22:42
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Blocked',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название клиента')),
            ],
            options={
                'verbose_name': 'Заблокирован',
                'verbose_name_plural': 'Заблокированы',
            },
        ),
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название бота')),
                ('token', models.TextField(verbose_name='Токен бота')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bots', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Бот',
                'verbose_name_plural': 'Боты',
            },
        ),
        migrations.CreateModel(
            name='BotUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название клиента')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='users', to='bot_app.Bot')),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
            },
        ),
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название чата')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chats', to='bot_app.Bot')),
            ],
            options={
                'verbose_name': 'Чат',
                'verbose_name_plural': 'Чаты',
            },
        ),
        migrations.CreateModel(
            name='ClientProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('current_action', models.CharField(default='', max_length=255)),
                ('current_action_edit', models.CharField(default='', max_length=255)),
                ('end_item_number', models.PositiveIntegerField(default=0)),
                ('bot_selected', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='bot_app.Bot')),
            ],
            options={
                'verbose_name': 'Профиль клиента',
                'verbose_name_plural': 'Профили клиентов',
            },
        ),
        migrations.CreateModel(
            name='CommandBot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название команды')),
                ('random_message', models.BooleanField(default=False, verbose_name='Возвращать случайное сообщение')),
                ('saved', models.BooleanField(default=False, verbose_name='Комманда сохранена')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='commands', to='bot_app.Bot')),
            ],
            options={
                'verbose_name': 'Команда',
                'verbose_name_plural': 'Команды',
            },
        ),
        migrations.CreateModel(
            name='CommandQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название вопроса')),
                ('type_content', models.CharField(choices=[('text', 'Текст'), ('photo', 'Фото')], max_length=255, verbose_name='Тип системного события')),
                ('command', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_questions', to='bot_app.CommandBot')),
            ],
            options={
                'verbose_name': 'Вопрос для соманды',
                'verbose_name_plural': 'Вопросы для команды',
            },
        ),
        migrations.AddField(
            model_name='clientprofile',
            name='current_command',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='bot_app.CommandBot'),
        ),
        migrations.AddField(
            model_name='clientprofile',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='blocked',
            name='bot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='blockeds', to='bot_app.Bot'),
        ),
    ]
