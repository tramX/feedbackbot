# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-01-13 12:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot_app', '0005_auto_20180113_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientprofile',
            name='message_multi_menu',
            field=models.CharField(default='new_message', max_length=255),
        ),
    ]
