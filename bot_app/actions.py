# Меню настройки бота
BOT_SETTINGS_MENU = {'name': '« Вернуться к настройкам', 'callback': 'botSetting'}
# Настройки бота приветствие
BOT_SETTINGS_HELLO = {'name': 'Приветствие', 'callback': 'bSHello'}
#
BOT_SETTINGS_USERS = {'name': 'Пользователи', 'callback': 'bSUsers'}
#
BOT_SETTINGS_CHATS = {'name': 'Чат', 'callback': 'bSChat'}
#
BOT_SETTINGS_BLACK_LISTS = {'name': 'Черный список', 'callback': 'bSBlackList'}
#
BOT_SETTINGS_DEL_BOT = {'name': 'Удалить бота', 'callback': 'bSDelBot'}
# Список ботов
BOT_SETTINGS_BOT_LIST = {'name': '« Вернуться к списку ботов', 'callback': 'botList'}
#
BOT_SETTINGS_FIRST_HELLO = {'name': 'Пер.прив', 'callback': 'bSFirstHi'}
#
BOT_SETTINGS_TWO_HELLO = {'name': 'Втор. прив', 'callback': 'bSTwoHi'}
#
BOT_SETTINGS_ADVERTISING = {'name': 'Реклама', 'callback': 'bSAdvertising'}
#
MY_BOTS = {'name': 'Меню', 'callback': 'myBots'}
#
BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS = {'name': 'Добавить кнопки', 'callback': 'bSFirstHelloAddButtons'}
BOT_SETTINGS_FIRST_HELLO_ADD_SMILES = {'name': 'Добавить смайлики', 'callback': 'bSFirstHelloAddSmiles'}
BOT_SETTINGS_FIRST_HELLO_DELAY = {'name': 'Задержка', 'callback': 'bSFirstHelloDelay'}
BOT_SETTINGS_FIRST_HELLO_PREVIEW = {'name': 'Предпросмотр', 'callback': 'bSFirstHelloPreview'}
BOT_SETTINGS_FIRST_HELLO_START_OVER = {'name': 'Начать сначала', 'callback': 'bSFirstHelloStartOver'}
BOT_SETTINGS_FIRST_HELLO_MARKDOWN = {'name': 'markdown', 'callback': 'bSFirstHelloMarkdown'}
BOT_SETTINGS_FIRST_HELLO_USERNAME = {'name': '@username', 'callback': 'bSFirstHelloUsername'}
#
BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS = {'name': 'Добавить кнопки', 'callback': 'bSTwoHelloAddButtons'}
BOT_SETTINGS_TWO_HELLO_ADD_SMILES = {'name': 'Добавить смайлики', 'callback': 'bSTwoHelloAddSmiles'}
BOT_SETTINGS_TWO_HELLO_DELAY = {'name': 'Задержка', 'callback': 'bSTwoHelloDelay'}
BOT_SETTINGS_TWO_HELLO_PREVIEW = {'name': 'Предпросмотр', 'callback': 'bSFirstHelloPreview'}
BOT_SETTINGS_TWO_HELLO_START_OVER = {'name': 'Начать сначала', 'callback': 'bSFirstHelloStartOver'}
BOT_SETTINGS_TWO_HELLO_MARKDOWN = {'name': 'markdown', 'callback': 'bSTwoHelloMarkdown'}
BOT_SETTINGS_TWO_HELLO_USERNAME = {'name': '@username', 'callback': 'bSTwoHelloUsername'}
# Рассылка
BOT_SETTINGS_USERS_NEWS_LETTER = {'name': 'Рассылка', 'callback': 'bSNewsLetter'}
BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS = {'name': 'Добавить кнопки', 'callback': 'bSNewsletterAddButtons'}
BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES = {'name': 'Добавить смайлики', 'callback': 'bSNewsletterAddSmiles'}
BOT_SETTINGS_USERS_NEWS_LETTER_SEND = {'name': 'Отправить', 'callback': 'bSNewsletterSend'}
BOT_SETTINGS_USERS_NEWS_LETTER_SEND_TIME = {'name': 'Время отправления', 'callback': 'bSNewsletterSendTime'}
BOT_SETTINGS_USERS_NEWS_LETTER_PREVIEW = {'name': 'Предпросмотр', 'callback': 'bSsNewsletterPreview'}
BOT_SETTINGS_USERS_NEWS_LETTER_START_OVER = {'name': 'Начать сначала', 'callback': 'bSNewsletterStartOver'}
BOT_SETTINGS_USERS_NEWS_LETTER_REGULAR_NEWS_LETTER = {'name': 'Регулярная расс',
                                                      'callback': 'bSNewsletterRegularNewsletter'}
#
BOT_SETTINGS_CHATS_BOT_OFF = {'name': 'Отключить бота в чате', 'callback': 'bSChatBotOff'}
BOT_SETTINGS_CHATS_BOT_RIGHTS = {'name': 'Права', 'callback': 'bSChatRights'}
# Команды
BOT_SETTINGS_COMMANDS = {'name': 'Команды', 'callback': 'bSComs'}
BOT_SETTINGS_COMMANDS_CREATE_COMMAND = {'name': 'Создать команду', 'callback': 'bSCCreateCommand'}
BOT_SETTINGS_COMMANDS_MY_COMMANDS = {'name': 'Мои команды', 'callback': 'bSCMyCommands'}
BOT_SETTINGS_COMMANDS_DEL_COMMANDS = {'name': 'Удалить все', 'callback': 'bSCDelAllCommands'}
BOT_SETTINGS_COMMANDS_RETURN_TO_COMMAND = {'name': '« Вернуться к команде', 'callback': 'bSCReturnToCommand'}
BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE = {'name': 'Создать команду', 'callback': 'bSCCreateCommandActive'}
BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION = {'name': 'Добавить вопрос',
                                                            'callback': 'bSCCreateCommandActiveAddQues'}
BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_RANDOM_MESSAGE = {'name': 'Случайное сообщение',
                                                                  'callback': 'bSCCreateCommandActiveRandMess'}
BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_SAVE = {'name': 'Сохранить', 'callback': 'bSCCreateCommandActiveSave'}
# Кнопки
BOT_SETTINGS_BUTTONS = {'name': 'Кнопки', 'callback': 'bSButtons'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON = {'name': 'Создать кнопку', 'callback': 'bSCCreateButton'}
BOT_SETTINGS_BUTTONS_MY_BUTTONS = {'name': 'Мои кнопки', 'callback': 'bSCYyButtons'}
BOT_SETTINGS_BUTTONS_DEL_BUTTONS = {'name': 'Удалить все', 'callback': 'bSCDelButtons'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN = {'name': 'В чате', 'callback': 'bSCCreateButtonInDown',
                                              'callback_param_name': 'bot_name',
                                              'alternative_name': {'create_button_in_down_chat': {'in_chat': 'В чате',
                                                                                            'down_chat': 'Под чатом'}}}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI = {'name': 'Новое сообщение', 'callback': 'bSCCreateButtonMulti',
                                            'callback_param_name': 'bot_name',
                                            'alternative_name': {
                                                'message_multi_menu': {'new_message': 'Новое сообщение',
                                                                       'multi_menu': 'Мультименю'}}}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION = {'name': 'Создать кнопку', 'callback': 'bSCCreateButtonAction'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_RANDOM = {'name': 'Случайный порядок',
                                                    'callback': 'bSCCreateButtonActionRand'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION = {'name': 'Добавить вопрос',
                                                          'callback': 'bSCCreateButtonActionAddQues'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD = {'name': 'Создать пароль',
                                                             'callback': 'bSCCreateButtonActionCrePass'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_STEP_BACK = {'name': '« Вернуться на ш назад',
                                                       'callback': 'bSCCreateButtonActionStepBack'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_RETURN_BUTTON = {'name': '« Вернуться к кнопке', 'callback': 'bSCCreateButtonAction'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD_ACTION = {'name': 'Создать пароль',
                                                                    'callback': 'bSCCreateButtonActionCrePassAct'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_DEL_PASSWORD = {'name': 'Удалить',
                                                          'callback': 'bSCCreateButtonActionDelPass'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_EDIT_PASSWORD = {'name': 'Редактировать',
                                                           'callback': 'bSCCreateButtonActionEditPass'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl = {'name': 'Обычный',
                                                                 'callback': 'bSCCreateButtonActionAddQuesN'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS = {'name': 'Развернутый',
                                                                     'callback': 'bSCCreateButtonActionAddQuesF'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS = {'name': 'С вариантами ответов',
                                                                                'callback':
                                                                                    'bSCCreateButtonActionAddQuesW'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE = {'name': 'Анкета',
                                                                        'callback': 'bSCCreateButtonActionAddQuesQ'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES = {'name': '« Вернуться к типу вопросов',
                                                                                'callback':
                                                                                    'bSCCreateButtonActionAddQues'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE = {'name': 'Сохранить кнопку',
                                                  'callback': 'bSCCreateButtonActionSave'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_SAVE = {'name': 'Сохранить',
                                           'callback': 'bSCCreateButtonSave'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT = {'name': 'Количество сообщений',
                                                                           'callback':
                                                                               'bSCCreateButtonActionAddQuesFCount'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT = {'name': 'Варианты ответов',
                                                                                    'callback':
                                                                                    'bSCCreateButtonActionAddQuesWACT'}
BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT = {'name': 'Анкета следующий вопрос',
                                                                             'callback':
                                                                                 'bSCCreateButtonActionAddQuesQNext'}
#
BOT_SETTINGS_BLACK_LISTS_UNBLOCKED_ALL = {'name': 'Разблокировать всех', 'callback': 'bSBlackListUnblockedAll'}
# Автопостинг
BOT_SETTINGS_AUTO_POSTING = {'name': 'Автопостинг', 'callback': 'bSAutoPosting'}
BOT_SETTINGS_AUTO_POSTING_CREATE_POST = {'name': 'Создать пост', 'callback': 'bSAutoPostingCP'}
BOT_SETTINGS_AUTO_POSTING_TWITTER = {'name': 'Twitter', 'callback': 'bSAutoPostingTwitter'}
BOT_SETTINGS_AUTO_POSTING_VK = {'name': 'VK', 'callback': 'bSAutoPostingVK'}
BOT_SETTINGS_AUTO_POSTING_YOUTUBE = {'name': 'YOUTUBE', 'callback': 'bSAutoPostingYoutube'}
BOT_SETTINGS_AUTO_POSTING_FACEBOOK = {'name': 'Facebook', 'callback': 'bSAutoPostingFacebook'}
BOT_SETTINGS_AUTO_POSTING_INSTAGRAM = {'name': 'Instagram', 'callback': 'bSAutoPostingInstagram'}
BOT_SETTINGS_AUTO_POSTING_RSS = {'name': 'RSS', 'callback': 'bSAutoPostingRSS'}
BOT_SETTINGS_AUTO_POSTING_CREATE_POST_SEND_DATA = {'name': 'Создаем пост', 'callback': 'bSAutoPostingCPSD'}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME = {'name': 'Время публикации', 'callback': 'bSAutoPostingPubTime'}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY = {'name': 'Дни публикации', 'callback': 'bSAutoPostingPubDay'}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_EVERY_DAY = {'name': '✓ Каждый день', 'callback': 'bSAutoPostingPubEveryDay',
                                                 'alternative_name':
                                                     {'on_off': {True: '✓ Каждый день', False: '× Каждый день'}}}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_IN_ON_DAY = {'name': '✓ Через день', 'callback': 'bSAutoPostingPubInOnDay',
                                                 'alternative_name':
                                                     {'on_off': {True: '✓ Через день', False: '× Через день'}}}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_MONDAY = {'name': '✓ Понедельник', 'callback': 'bSAutoPostingPubMonday',
                                              'alternative_name': {
                                                  'on_off': {True: '✓ Понедельник', False: '× Понедельник'}}}
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TUESDAY = {'name': '✓ Вторник', 'callback': 'bSAutoPostingPubTuesday',
                                               'alternative_name': {
                                                   'on_off': {True: '✓ Вторник', False: '× Вторник'}}
                                               }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_WEDNESDAY = {'name': '✓ Среда', 'callback': 'bSAutoPostingPubWednesday',
                                                 'alternative_name': {
                                                     'on_off': {True: '✓ Среда', False: '× Среда'}}
                                                 }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_THURSDAY = {'name': '✓ Четверг', 'callback': 'bSAutoPostingPubThursday',
                                                'alternative_name': {
                                                    'on_off': {True: '✓ Четверг', False: '× Четверг'}}
                                                }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_FRIDAY = {'name': '✓ Пятница', 'callback': 'bSAutoPostingPubFriday',
                                              'alternative_name': {
                                                  'on_off': {True: '✓ Пятница', False: '× Пятница'}}
                                              }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SATURDAY = {'name': '✓ Суббота', 'callback': 'bSAutoPostingPubSaturday',
                                                'alternative_name': {
                                                    'on_off': {True: '✓ Суббота', False: '× Суббота'}}
                                                }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SUNDAY = {'name': '✓ Воскресенье', 'callback': 'bSAutoPostingPubSunday',
                                              'alternative_name': {
                                                  'on_off': {True: '✓ Воскресенье', False: '× Воскресенье'}}
                                              }
BOT_SETTINGS_AUTO_POSTING_PUBLISHED_OFF = {'name': '× Отключить', 'callback': 'bSAutoPostingPubOff',
                                           'alternative_name': {
                                               'on_off': {True: '✓ Отключить', False: '× Отключить'}}
                                           }
BOT_SETTINGS_AUTO_POSTING_SAVE = {'name': 'Сохранить', 'callback': 'botSetting'}
BOT_SETTINGS_AUTO_POSTING_ADD_BUTTONS = {'name': 'Добавить кнопки', 'callback': 'bSAutoPostingAddButtons'}
BOT_SETTINGS_AUTO_POSTING_ADD_SMILES = {'name': 'Добавить смайлики', 'callback': 'bSAutoPostingAddSmiles'}
BOT_SETTINGS_AUTO_POSTING_PREVIEW = {'name': 'Предпросмотр', 'callback': 'bSAutoPostingPreview'}
BOT_SETTINGS_AUTO_POSTING_START_OVER = {'name': 'Начать сначала', 'callback': 'bSAutoPostingStartOver'}
BOT_SETTINGS_AUTO_POSTING_TEMPORARY_UNSUBSRIPTION = {'name': 'Временная отписка', 'callback': 'bSAutoPostingTempUn'}

keyboard_entities = {
    BOT_SETTINGS_MENU.get('callback'): [
        [BOT_SETTINGS_HELLO, BOT_SETTINGS_USERS],
        [BOT_SETTINGS_CHATS, BOT_SETTINGS_COMMANDS],
        [BOT_SETTINGS_BUTTONS, BOT_SETTINGS_BLACK_LISTS],
        [BOT_SETTINGS_AUTO_POSTING, BOT_SETTINGS_DEL_BOT],
        [BOT_SETTINGS_BOT_LIST]
    ],
    BOT_SETTINGS_HELLO.get('callback'): [
        [BOT_SETTINGS_HELLO, BOT_SETTINGS_TWO_HELLO],
        [BOT_SETTINGS_ADVERTISING, MY_BOTS],
        [BOT_SETTINGS_FIRST_HELLO, BOT_SETTINGS_TWO_HELLO],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO.get('callback'): [
        [BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS, BOT_SETTINGS_FIRST_HELLO_ADD_SMILES],
        [BOT_SETTINGS_FIRST_HELLO_DELAY, BOT_SETTINGS_FIRST_HELLO_PREVIEW],
        [BOT_SETTINGS_FIRST_HELLO_START_OVER, BOT_SETTINGS_FIRST_HELLO_MARKDOWN],
        [BOT_SETTINGS_FIRST_HELLO_USERNAME, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO.get('callback'): [
        [BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS, BOT_SETTINGS_TWO_HELLO_ADD_SMILES],
        [BOT_SETTINGS_TWO_HELLO_DELAY, BOT_SETTINGS_TWO_HELLO_PREVIEW],
        [BOT_SETTINGS_TWO_HELLO_START_OVER, BOT_SETTINGS_TWO_HELLO_MARKDOWN],
        [BOT_SETTINGS_TWO_HELLO_USERNAME, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS.get('callback'): [
        [BOT_SETTINGS_USERS_NEWS_LETTER],
        [BOT_SETTINGS_MENU],
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'): [
        [BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS, BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES],
        [BOT_SETTINGS_USERS_NEWS_LETTER_SEND, BOT_SETTINGS_USERS_NEWS_LETTER_SEND_TIME],
        [BOT_SETTINGS_USERS_NEWS_LETTER_PREVIEW, BOT_SETTINGS_USERS_NEWS_LETTER_START_OVER],
        [BOT_SETTINGS_USERS_NEWS_LETTER_REGULAR_NEWS_LETTER, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_CHATS.get('callback'): [
        [BOT_SETTINGS_CHATS_BOT_OFF],
        [BOT_SETTINGS_CHATS_BOT_RIGHTS],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS.get('callback'): [
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND, BOT_SETTINGS_COMMANDS_MY_COMMANDS],
        [BOT_SETTINGS_COMMANDS_DEL_COMMANDS, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS_MY_COMMANDS.get('callback'): [
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND],
        [BOT_SETTINGS_COMMANDS_RETURN_TO_COMMAND],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback'): [
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION],
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_RANDOM_MESSAGE],
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_SAVE],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION.get('callback'): [
        [BOT_SETTINGS_COMMANDS_RETURN_TO_COMMAND],
    ],
    BOT_SETTINGS_DEL_BOT.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_COMMANDS_RETURN_TO_COMMAND.get('callback'): [
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION],
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_RANDOM_MESSAGE],
        [BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_SAVE],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON, BOT_SETTINGS_BUTTONS_MY_BUTTONS],
        [BOT_SETTINGS_BUTTONS_DEL_BUTTONS, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BLACK_LISTS.get('callback'): [
        [BOT_SETTINGS_BLACK_LISTS_UNBLOCKED_ALL],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING.get('callback'): [
        [BOT_SETTINGS_AUTO_POSTING_CREATE_POST, BOT_SETTINGS_AUTO_POSTING_TWITTER],
        [BOT_SETTINGS_AUTO_POSTING_VK, BOT_SETTINGS_AUTO_POSTING_YOUTUBE],
        [BOT_SETTINGS_AUTO_POSTING_FACEBOOK, BOT_SETTINGS_AUTO_POSTING_INSTAGRAM],
        [BOT_SETTINGS_AUTO_POSTING_RSS, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_RANDOM, BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD, BOT_SETTINGS_BUTTONS_CREATE_BUTTON],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_STEP_BACK, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_MY_BUTTONS.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_RETURN_BUTTON]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD_ACTION.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_DEL_PASSWORD],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_EDIT_PASSWORD],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_RETURN_BUTTON]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_EDIT_PASSWORD.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_RETURN_BUTTON]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl,
         BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS,
         BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_RANDOM, BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD, BOT_SETTINGS_BUTTONS_CREATE_BUTTON],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_SAVE],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_STEP_BACK, BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES]
    ],
    BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get('callback'): [
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_SAVE],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_RETURN_QUESTION_TYPES],
        [BOT_SETTINGS_BUTTONS_CREATE_BUTTON_RETURN_BUTTON],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO_ADD_SMILES.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO_DELAY.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO_START_OVER.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_FIRST_HELLO_MARKDOWN.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO_ADD_SMILES.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO_DELAY.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO_START_OVER.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_TWO_HELLO_MARKDOWN.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER_SEND_TIME.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER_START_OVER.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_USERS_NEWS_LETTER_REGULAR_NEWS_LETTER.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_CHATS_BOT_OFF.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY.get('callback'): [
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_EVERY_DAY, BOT_SETTINGS_AUTO_POSTING_PUBLISHED_IN_ON_DAY],
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_MONDAY, BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TUESDAY],
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_WEDNESDAY, BOT_SETTINGS_AUTO_POSTING_PUBLISHED_THURSDAY],
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_FRIDAY, BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SATURDAY],
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SUNDAY, BOT_SETTINGS_AUTO_POSTING_PUBLISHED_OFF],
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_CREATE_POST.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_TWITTER.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_VK.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_YOUTUBE.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_FACEBOOK.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_INSTAGRAM.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_RSS.get('callback'): [
        [BOT_SETTINGS_MENU]
    ],
    BOT_SETTINGS_AUTO_POSTING_CREATE_POST_SEND_DATA.get('callback'): [
        [BOT_SETTINGS_AUTO_POSTING_ADD_BUTTONS, BOT_SETTINGS_AUTO_POSTING_ADD_SMILES],
        [BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME, BOT_SETTINGS_AUTO_POSTING_PREVIEW],
        [BOT_SETTINGS_AUTO_POSTING_START_OVER, BOT_SETTINGS_AUTO_POSTING_TEMPORARY_UNSUBSRIPTION]
    ],
}
