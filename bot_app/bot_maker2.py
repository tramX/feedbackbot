import re
import telebot
from datetime import datetime

from bot_app import models
from . import actions

IN_DOWN_CHAT = (
    ('in_chat', 'В чате'),
    ('down_chat', 'Под чатом'),
)
MESSAGE_MULTI_MENU = (
    ('new_message', 'Новое сообщение'),
    ('multi_menu', 'Мультименю'),
)


class BotMaker:
    def __init__(self, master_bot, user, message_id):
        print('BotMaker', user)
        self.bot_list = models.Bot.objects.filter(client=user)
        self.master_bot = master_bot
        self.user = user
        self.message_id = message_id

        # Список методов, которые отдают данные в телеграм
        self.work_methods = {
            actions.BOT_SETTINGS_MENU.get('callback'): {'method': self.bot_settings_menu,
                                                        'message': 'Бот: @{bot_name}'},
            actions.BOT_SETTINGS_HELLO.get('callback'): {'method': self.bot_setting, 'message': 'Бот: @{bot_name}'},
            actions.BOT_SETTINGS_USERS.get('callback'): {'method': self.bot_setting,
                                                         'message': 'Статистика пользователей бота {bot_name} Всего \n'
                                                                    'пользователей: {all_users} Заблокировали бот: '
                                                                    '{blocked_bot} \n '
                                                                    'Счетчик тех, кто заблокировал бот \n '
                                                                    'обновляется после каждой рассылки'},
            actions.BOT_SETTINGS_COMMANDS.get('callback'): {'method': self.bot_setting, 'message': 'Текст'},
            actions.BOT_SETTINGS_COMMANDS_MY_COMMANDS.get('callback'): {'method': self.my_commands,
                                                                        'message': 'Мои комманды'},
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'): {'method': self.bot_setting,
                                                                           'message': 'Укажите название для новой '
                                                                                      'комманды. \n Команда может '
                                                                                      'содержать '
                                                                                      ' латинские буквы цыфры и "_" '
                                                                                      '\n Примеры команд \n'
                                                                                      '/website \n /pricelist '
                                                                                      '\n /contacts \n /best_music'},
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback'): {'method': self.bot_setting, 'message':
                'Отправте боту все, что хотите добавить к команде '
                'и намите Сохранить'},
            actions.BOT_SETTINGS_COMMANDS_DEL_COMMANDS.get('callback'): {
                'method': self.bot_settings_commands_del_all_commands, 'message': 'Все команды удалены'},
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION.get('callback'): {
                'method': self.bot_setting, 'message': 'Введите вопрос:'},
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_RANDOM_MESSAGE.get('callback'): {
                'method': self.command_random_message, 'message': 'Случайное сообщение'},
            actions.BOT_SETTINGS_COMMANDS_RETURN_TO_COMMAND.get('callback'): {'method': self.return_to_command,
                                                                              'message': 'Вернуться к команде'},
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_SAVE.get('callback'): {'method': self.save_command,
                                                                                       'message': 'Вернуться к команде'},
            actions.BOT_SETTINGS_BOT_LIST.get('callback'): {'method': self.my_bots, 'message': 'Мои каналы'},
            actions.BOT_SETTINGS_DEL_BOT.get('callback'): {'method': self.bot_setting,
                                                           'message': 'Чтобы удалить бота, отправте сообщение '
                                                                      '"Удалить бота"'},
            actions.BOT_SETTINGS_CHATS.get('callback'): {'method': self.bot_setting,
                                                         'message': 'Ваш бот {bot_name} находится в чате chat1'},
            actions.BOT_SETTINGS_BUTTONS.get('callback'): {'method': self.bot_setting, 'message': 'Кнопки'},
            actions.BOT_SETTINGS_BLACK_LISTS.get('callback'): {'method': self.bot_setting,
                                                               'message': 'Статистика пользователей бота {bot_name} Всего '
                                                                          'пользователей: {all_users}  Заблокировано: '
                                                                          '{black_lists}'},
            actions.BOT_SETTINGS_AUTO_POSTING.get('callback'): {'method': self.bot_setting,
                                                                'message': 'Вы можете добавлять автопостинг из Twitter, '
                                                                           'YouTube,'
                                                                           ' VK или любого RSS канала. Какой сервис вам '
                                                                           'нужен?'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON.get('callback'): {'method': self.bot_setting,
                                                                         'message': 'Укажите название кнопки'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN.get('callback'): {'method': self.button_in_down_chat,
                                                                                 'message': 'Укажите название кнопки'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI.get('callback'): {'method': self.message_multi_menu,
                                                                               'message': 'Укажите название кнопки'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback'): {'method': self.bot_setting,
                                                                                'message': 'Отправте боту все, что '
                                                                                           'хотите добавить к команде '
                                                                                           'и намите Сохранить'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_RANDOM.get('callback'): {
                'method': self.create_new_button_random_order,
                'message': 'Кнопки'},
            actions.BOT_SETTINGS_BUTTONS_MY_BUTTONS.get('callback'): {'method': self.my_buttons,
                                                                      'message': 'Мои кнопки'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD.get('callback'): {
                'method': self.bot_setting,
                'message': 'Введите пароль: '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD_ACTION.get('callback'): {
                'method': self.bot_setting,
                'message': 'Ваш пароль: {btn_password}'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_EDIT_PASSWORD.get('callback'): {
                'method': self.bot_setting,
                'message': 'Введите пароль: '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION.get('callback'): {
                'method': self.bot_setting,
                'message': 'Выберите тип вопроса '},

            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl.get('callback'): {
                'method': self.bot_setting,
                'message': 'Выбран тип вопроса '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS.get('callback'): {
                'method': self.bot_setting,
                'message': 'Выбран тип вопроса '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS.get('callback'): {
                'method': self.bot_setting,
                'message': 'Выбран тип вопроса '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE.get('callback'): {
                'method': self.bot_setting,
                'message': 'Введите первый вопрос '},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get('callback'): {
                'method': self.bot_setting,
                'message': 'Отправте боту все, что хотите добавить к команде и намите Сохранить'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_SAVE.get('callback'): {'method': self.save_button,
                                                                              'message': 'Сохранение'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get('callback'):
                {'method': self.bot_setting, 'message': 'Введите количество сообщений, которое пользователь'
                                                        ' может прислать в ответ на вопрос'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get('callback'):
                {'method': self.bot_setting, 'message': 'Введите варианты ответов, каждый с новой строки'},
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get('callback'):
                {'method': self.bot_setting, 'message': 'Введите следующий вопрос'},
            actions.BOT_SETTINGS_BUTTONS_DEL_BUTTONS.get('callback'): {
                'method': self.bot_settings_del_all_buttons,
                'message': 'Удалить все кнопки'},
            actions.BOT_SETTINGS_FIRST_HELLO.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_TWO_HELLO.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_FIRST_HELLO_START_OVER.get('callback'): {
                'method': self.hello_start_over,
                'message': 'Начать сначала'},
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_SMILES.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_FIRST_HELLO_DELAY.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_FIRST_HELLO_MARKDOWN.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_TWO_HELLO_START_OVER.get('callback'): {
                'method': self.hello_start_over,
                'message': 'Начать сначала'},
            actions.BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_TWO_HELLO_ADD_SMILES.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_TWO_HELLO_DELAY.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_TWO_HELLO_MARKDOWN.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_SEND_TIME.get('callback'): {
                'method': self.bot_setting,
                'message': 'Укажите время в формате: 12:00'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES.get('callback'): {
                'method': self.bot_setting,
                'message': 'Пришлите информацию'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_START_OVER.get('callback'): {
                'method': self.news_letter_start_over,
                'message': 'Начать сначала'},
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_REGULAR_NEWS_LETTER.get('callback'): {
                'method': self.news_letter_is_regular,
                'message': 'Регулярная рассылка'},
            actions.BOT_SETTINGS_CHATS_BOT_OFF.get('callback'): {
                'method': self.bot_chat_off,
                'message': 'Отключить бота в чате'},
            actions.BOT_SETTINGS_BLACK_LISTS_UNBLOCKED_ALL.get('callback'): {
                'method': self.black_list_unblocked_all,
                'message': 'Разблокировать всех'},
            actions.BOT_SETTINGS_AUTO_POSTING_CREATE_POST.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Введите текст'},
            actions.BOT_SETTINGS_AUTO_POSTING_TWITTER.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://twitter.com/'},
            actions.BOT_SETTINGS_AUTO_POSTING_VK.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://vk.com'},
            actions.BOT_SETTINGS_AUTO_POSTING_YOUTUBE.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://www.youtube.com'},
            actions.BOT_SETTINGS_AUTO_POSTING_FACEBOOK.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://www.facebook.com/'},
            actions.BOT_SETTINGS_AUTO_POSTING_INSTAGRAM.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://www.instagram.com'},
            actions.BOT_SETTINGS_AUTO_POSTING_RSS.get('callback'): {
                'method': self.auto_posting_select_source,
                'message': 'Укажите источник: https://rss.com'},
            actions.BOT_SETTINGS_AUTO_POSTING_CREATE_POST_SEND_DATA.get('callback'): {
                'method': self.bot_setting,
                'message': 'Информация сохранена. Вы можете прислать еще'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME.get('callback'): {
                'method': self.bot_setting,
                'message': 'Введите время автопостинга в формате 12:00 \n'
                           'Если пост публикуется несколько \n'
                           'раз в сутки. Каждое время \n '
                           'вводите с новой строки'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY.get('callback'): {
                'method': self.bot_setting,
                'message': 'Выбрать дни публикации'},

            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_EVERY_DAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_IN_ON_DAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_MONDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TUESDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_WEDNESDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_THURSDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_FRIDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SATURDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SUNDAY.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_OFF.get('callback'): {
                'method': self.auto_posting_save_selected_day,
                'message': 'Выбрать дни публикации'},

        }

        # Список методов, которые принимают данные от клиента из телеграм
        self.edit_methods = {
            '': self.edit_method_default,
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'): self.create_new_command,
            actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE_ADD_QUESTION.get(
                'callback'): self.add_question_to_command,
            actions.BOT_SETTINGS_DEL_BOT.get('callback'): self.del_bot,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON.get('callback'): self.create_new_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_IN_DOWN.get('callback'): self.create_new_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_MULTI.get('callback'): self.create_new_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD.get(
                'callback'): self.button_create_password,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_EDIT_PASSWORD.get(
                'callback'): self.button_create_password,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl.get(
                'callback'): self.add_question_to_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS.get(
                'callback'): self.add_question_to_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS.get(
                'callback'): self.add_question_to_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE.get(
                'callback'): self.add_question_to_button,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get(
                'callback'): self.add_question_to_button_count_messages,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get(
                'callback'): self.add_question_to_button_answers_variant,
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get('callback'):
                self.add_question_to_button_questionnarie_next,
            actions.BOT_SETTINGS_FIRST_HELLO.get('callback'):
                self.add_first_hello_text,
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_SMILES.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_FIRST_HELLO_DELAY.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_FIRST_HELLO_MARKDOWN.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_TWO_HELLO.get('callback'):
                self.add_two_hello_text,
            actions.BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_TWO_HELLO_ADD_SMILES.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_TWO_HELLO_DELAY.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_TWO_HELLO_MARKDOWN.get('callback'):
                self.add_first_hello_actions_save,
            actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'):
                self.add_news_letter_text,
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_SEND_TIME.get('callback'):
                self.add_news_letter_sent_time,
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS.get('callback'):
                self.add_news_letter_actions_save,
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES.get('callback'):
                self.add_news_letter_actions_save,
            actions.BOT_SETTINGS_AUTO_POSTING_TWITTER.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_VK.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_YOUTUBE.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_FACEBOOK.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_INSTAGRAM.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_RSS.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_CREATE_POST.get('callback'):
                self.auto_posting_select_source_save,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME.get('callback'):
                self.auto_posting_save_publication_times
        }

    def keyboard_item_list(self, list, step, netx_or_previous=None, last_button=None, action=None):
        if netx_or_previous == 'next':
            self.user.profile.end_item_number = self.user.profile.end_item_number + step
            self.user.profile.save()

        if netx_or_previous == 'previous':
            self.user.profile.end_item_number = self.user.profile.end_item_number - step
            self.user.profile.save()

        if netx_or_previous == None:
            self.user.profile.end_item_number = 0
            self.user.profile.save()

        items_keyboard = telebot.types.InlineKeyboardMarkup()
        items_list = [{'name': item.name, 'callback_data': '{}:{}'.format(action.get('callback'), item.name)} for item
                      in list]

        items_list_to_keyboard = items_list[
                                 self.user.profile.end_item_number:(self.user.profile.end_item_number + step)]

        if last_button:
            items_list_to_keyboard.append(last_button)

        items_keyboard.add(
            *[telebot.types.InlineKeyboardButton(text=item.get('name'), callback_data=item.get('callback_data'))
              for item in items_list_to_keyboard])

        if len(list) > (self.user.profile.end_item_number + step) and self.user.profile.end_item_number > 0:
            items_keyboard.add(
                *[telebot.types.InlineKeyboardButton(text="«",
                                                     callback_data="{}:previous".format(action.get('callback'))),
                  telebot.types.InlineKeyboardButton(text="»",
                                                     callback_data="{}:next".format(action.get('callback')))
                  ]
            )

        if len(list) > (self.user.profile.end_item_number + step) and self.user.profile.end_item_number == 0:
            items_keyboard.add(
                telebot.types.InlineKeyboardButton(text="»", callback_data="{}:next".format(action.get('callback'))))

        if self.user.profile.end_item_number > 0 and (self.user.profile.end_item_number + step) > len(list):
            items_keyboard.add(
                telebot.types.InlineKeyboardButton(text="«",
                                                   callback_data="{}:previous".format(action.get('callback'))))

        return items_keyboard

    def edit_method_default(self, message):
        print('DEFAULT EDIT METHOD')

    def send_message(self, chat_id, text, keybord, message=None):
        # self.master_bot.send_message(chat_id, text, reply_markup=keybord)
        # if message != None:
        #    self.master_bot.edit_message_text(text, chat_id=chat_id,
        #                                      message_id=message.message.message_id, reply_markup=keybord)
        # self.master_bot.send_message(chat_id, text, reply_markup=keybord)
        try:
            self.master_bot.edit_message_text(text, chat_id=chat_id, message_id=self.message_id, reply_markup=keybord)
            # self.master_bot.delete_message(chat_id = chat_id, message_id = self.message_id)
            # self.master_bot.send_message(chat_id, text, reply_markup=keybord)
        except:
            try:
                mid = int(self.message_id) - 1
                self.master_bot.delete_message(chat_id=chat_id, message_id=mid)
                self.master_bot.send_message(chat_id, text, reply_markup=keybord)
            except:
                self.master_bot.send_message(chat_id, text, reply_markup=keybord)
                # try:

                # self.master_bot.edit_message_text(text, chat_id=chat_id, message_id=self.message_id,
                # reply_markup=keybord)
                # except:
                #    print('EXEPT', self.message_id)
                #    try:
                #        mid = int(self.message_id)-1
                #        self.master_bot.edit_message_text(text, chat_id=chat_id, message_id=mid,
                #                                          reply_markup=keybord)
                #    except:
                #        self.master_bot.send_message(chat_id, text, reply_markup=keybord)

    def get_work_method(self, name):
        return self.work_methods.get(name).get('method')

    def alternative_name_button(self, button):
        if button.get('alternative_name'):
            try:
                alternative_name_key = list(button.get('alternative_name').keys())[0]
                alternative_name_value = getattr(self.user.profile, list(button.get('alternative_name').keys())[0])
                return telebot.types.InlineKeyboardButton(text=button.get('alternative_name').get(alternative_name_key)
                                                          .get(alternative_name_value),
                                                          callback_data='{}:None'.format(button.get('callback')))
            except:
                return telebot.types.InlineKeyboardButton(text=button.get('name'),
                                                          callback_data='{}:None'.format(button.get('callback')))
        else:
            return telebot.types.InlineKeyboardButton(text=button.get('name'),
                                                      callback_data='{}:None'.format(button.get('callback')))

    def create_message(self, action, kwarg_messages):
        message = self.work_methods.get(action).get('message')
        # if isinstance(message, dict):
        #   if kwarg_messages.get(message.get('key')) != None:
        #        return message.get('yes').format(**kwarg_messages)
        #    else:
        #        return message.get('no').format(**kwarg_messages)
        return self.work_methods.get(action).get('message').format(**kwarg_messages)

    # Метод создает клавиатуры в зависимости от текущего состояния бота.
    def bot_setting_keyboard(self, **kwargs):
        bot_setting = telebot.types.InlineKeyboardMarkup()
        for button_row in actions.keyboard_entities.get(self.user.profile.current_action):
            bot_setting.add(
                *[self.alternative_name_button(button) for button in button_row])
        return bot_setting

    def my_bots(self, chat_id, message):
        work_bot_list = []
        for bot in self.bot_list:
            try:
                b = telebot.TeleBot(bot.token)
                b.get_me()
                work_bot_list.append(bot.id)
            except:
                pass

        self.bot_list = models.Bot.objects.filter(id__in=work_bot_list)
        self.current_action = actions.BOT_SETTINGS_MENU.get('callback')
        self.user.profile.current_action = actions.BOT_SETTINGS_MENU.get('callback')
        self.user.profile.save()
        last_button = {'name': 'Все боты', 'callback_data': 'allBots:'}
        text = 'Количество ботов: {}'.format(len(self.bot_list))
        keybord = self.keyboard_item_list(self.bot_list, 20, netx_or_previous=None, last_button=last_button,
                                          action=actions.BOT_SETTINGS_MENU)
        self.send_message(chat_id, text, keybord, message)

    def bot_settings_menu(self, chat_id, message):
        action, param = message.data.split(':')

        if param in list(self.bot_list.values_list('name', flat=True)):
            self.bot_selected = models.Bot.objects.get(name=param)
            self.user.profile.bot_selected = self.bot_selected
            self.user.profile.save()

        try:
            btn_password = self.user.profile.current_button.password
        except:
            btn_password = ''

        kwarg_messages = {'bot_name': '@{}'.format(self.user.profile.bot_selected.name),
                          'all_users': self.user.profile.bot_selected.users.all().count(),
                          'blocked_bot': self.user.profile.bot_selected.blockeds.all().count(),
                          'chat': self.user.profile.bot_selected.chats.all().count(),
                          'password': 'password', 'btn_password': btn_password,
                          'black_lists': self.user.profile.bot_selected.black_lists.all().count()}

        text = self.create_message(self.user.profile.current_action, kwarg_messages)
        keybord = self.bot_setting_keyboard(action=self.user.profile.current_action, bot_name=param, chat_id=chat_id,
                                            message=message)
        self.send_message(chat_id, text, keybord, message)

    def bot_setting(self, chat_id, message, action_name=None):

        try:
            btn_password = self.user.profile.current_button.password
        except:
            btn_password = ''

        kwarg_messages = {'bot_name': self.user.profile.bot_selected.name,
                          'all_users': self.user.profile.bot_selected.users.all().count(),
                          'blocked_bot': self.user.profile.bot_selected.blockeds.all().count(),
                          'chat': self.user.profile.bot_selected.chats.all().count(),
                          'password': 'password', 'btn_password': btn_password,
                          'black_lists': self.user.profile.bot_selected.black_lists.all().count()}
        text = self.create_message(self.user.profile.current_action, kwarg_messages)
        keybord = self.bot_setting_keyboard(action=self.user.profile.current_action,
                                            bot_name=self.user.profile.bot_selected.name, chat_id=chat_id,
                                            message=message)
        self.send_message(chat_id, text, keybord, message)

    def my_commands(self, chat_id, message):
        action, param = message.data.split(':')
        if param in ['previous', 'next']:
            netx_or_previous = param
        else:
            netx_or_previous = None

        self.current_action = actions.BOT_SETTINGS_COMMANDS_MY_COMMANDS.get('callback')
        self.user.profile.current_action = self.current_action
        self.user.profile.save()
        self.commands_list = self.user.profile.bot_selected.commands.all()
        if len(self.commands_list) == 0:
            text = 'У вас нету команд'
        else:
            text = 'Мои команды'
        keybord = self.keyboard_item_list(self.commands_list, 21, netx_or_previous=netx_or_previous, last_button=None,
                                          action=actions.BOT_SETTINGS_COMMANDS_MY_COMMANDS)

        for button_row in actions.keyboard_entities.get(self.current_action):
            keybord.add(
                *[telebot.types.InlineKeyboardButton(text=button.get('name'),
                                                     callback_data='{}:None'.format(button.get('callback')))
                  for button in button_row])

        self.send_message(chat_id, text, keybord, message)

    def bot_settings_commands_del_all_commands(self, chat_id, message):
        for command in self.user.profile.bot_selected.commands.all():
            command.delete()

        self.user.profile.current_action = actions.BOT_SETTINGS_COMMANDS.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_COMMANDS.get('callback'))

    def command_random_message(self, chat_id, message):
        # comm = models.CommandBot.objects.get(id=self.current_command_id)
        # comm.random_message = True
        # comm.save()
        self.user.profile.current_command.random_message = True
        self.user.profile.current_command.save()
        self.user.profile.current_action = actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback')
        self.user.profile.save

        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback'))

    def return_to_command(self, chat_id, message):
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_COMMANDS.get('callback'))

    def save_command(self, chat_id, message):
        # comm = models.CommandBot.objects.get(id=self.current_command_id)
        # comm.saved = True
        # comm.save()
        self.user.profile.current_command.saved = True
        self.user.profile.current_command.save()
        self.user.profile.current_action = actions.BOT_SETTINGS_COMMANDS.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_COMMANDS.get('callback'))

    def del_bot(self, message):
        if message.text == 'Удалить бота':
            # self.user.profile.current_action = actions.BOT_SETTINGS_BOT_LIST.get('callback')
            # self.user.profile.current_action_edit = ''
            # self.user.profile.save()
            bot = self.user.profile.bot_selected
            self.user.profile.bot_selected = None
            self.user.profile.current_action_edit = ''
            self.user.profile.save()
            bot.delete()
            last_button = {'name': 'Все боты', 'callback_data': 'allBots:'}
            text = 'Количество ботов: {}'.format(len(self.bot_list))
            keybord = self.keyboard_item_list(self.bot_list, 20, netx_or_previous=None, last_button=last_button,
                                              action=actions.BOT_SETTINGS_MENU)
            self.send_message(message.chat.id, text, keybord, message)
        else:
            self.bot_setting(message.chat.id, message,
                             action_name=actions.BOT_SETTINGS_DEL_BOT.get('callback'))

    def add_question_to_command(self, message):
        if message.text:
            models.CommandQuestion.objects.create(command=self.user.profile.current_command, name=message.text,
                                                  type_content='text')

        if message.photo:
            models.CommandQuestion.objects.create(command=self.user.profile.current_command, name='photo.jpg',
                                                  type_content='photo')
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback'))

    def create_new_command(self, message):
        if models.CommandBot.objects.filter(bot=self.user.profile.bot_selected, name=message.text).count() > 0:
            self.master_bot.send_message(message.chat.id, 'Данная команда уже была добавлена')
            self.bot_setting(message.chat.id, message,
                             action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'))
        else:
            if message.text[:1] != '/':
                self.master_bot.send_message(message.chat.id, 'Комманда должна начинаться с символа /')
                self.bot_setting(message.chat.id, message,
                                 action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'))
            else:
                if re.match(r"^[a-zA-Z0-9_/]+$", message.text) != None:
                    new_command = models.CommandBot.objects.create(bot=self.user.profile.bot_selected,
                                                                   name=message.text)
                    self.user.profile.current_command = new_command
                    self.user.profile.current_action = actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get(
                        'callback')
                    self.user.profile.current_action_edit = ''
                    self.user.profile.save()
                    self.message_id
                    # self.master_bot.send_message(message.chat.id, 'Команда добавлена')
                    self.bot_setting(message.chat.id, message,
                                     action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND_ACTIVE.get('callback'))

                else:
                    self.master_bot.send_message(message.chat.id, 'Команда может содержать латинские буквы цыфры и "_"')
                    self.bot_setting(message.chat.id, message,
                                     action_name=actions.BOT_SETTINGS_COMMANDS_CREATE_COMMAND.get('callback'))

    def button_in_down_chat(self, chat_id, message):
        if self.user.profile.create_button_in_down_chat == 'in_chat':
            self.user.profile.create_button_in_down_chat = 'down_chat'
        else:
            self.user.profile.create_button_in_down_chat = 'in_chat'
        self.user.profile.save()

        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON.get('callback'))

    def message_multi_menu(self, chat_id, message):
        if self.user.profile.message_multi_menu == 'new_message':
            self.user.profile.message_multi_menu = 'multi_menu'
        else:
            self.user.profile.message_multi_menu = 'new_message'
        self.user.profile.save()

        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON.get('callback'))

    def create_new_button(self, message):
        new_button = models.ButtonBot.objects.create(bot=self.user.profile.bot_selected, name=message.text)
        self.user.profile.current_button = new_button
        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback')
        self.user.profile.current_action_edit = ''
        self.user.profile.save()
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback'))

    def create_new_button_random_order(self, chat_id, message):
        self.user.profile.current_button.random_message = True
        self.user.profile.current_button.save()
        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback')
        self.user.profile.current_action_edit = ''
        self.user.profile.save()
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback'))

    def my_buttons(self, chat_id, message):
        action, param = message.data.split(':')
        if param in ['previous', 'next']:
            netx_or_previous = param
        else:
            netx_or_previous = None

        self.current_action = actions.BOT_SETTINGS_BUTTONS_MY_BUTTONS.get('callback')
        self.user.profile.current_action = self.current_action
        self.user.profile.save()
        self.buttons_list = self.user.profile.bot_selected.buttons.filter(saved=True)
        if len(self.buttons_list) == 0:
            text = 'У вас нет кнопок'
        else:
            text = 'Мои кнопки'

        keybord = self.keyboard_item_list(self.buttons_list, 21, netx_or_previous=netx_or_previous, last_button=None,
                                          action=actions.BOT_SETTINGS_BUTTONS_MY_BUTTONS)

        for button_row in actions.keyboard_entities.get(self.current_action):
            keybord.add(
                *[telebot.types.InlineKeyboardButton(text=button.get('name'),
                                                     callback_data='{}:None'.format(button.get('callback')))
                  for button in button_row])

        self.send_message(chat_id, text, keybord, message)

    def button_create_password(self, message):
        self.user.profile.current_button.password = message.text
        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD_ACTION.get(
            'callback')
        self.user.profile.current_action_edit = ''
        self.user.profile.save()
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_CREATE_PASSWORD_ACTION.get(
                             'callback'))

    def button_del_password(self, chat_id, message):
        self.user.profile.current_button.password = None
        self.user.profile.current_button.save()
        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback')

        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION.get('callback'))

    def select_type_question_to_button(self, chat_id, message):
        action, param = message.data.split(':')
        question_types = {
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl.get('callback'):
                'normal',
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS.get('callback'):
                'full_dress',
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS.get('callback'):
                'with_variants_of_answers',
            actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE.get('callback'):
                'questionnaire'
        }

    def add_question_to_button(self, message):
        if self.user.profile.current_action == \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_NORMAl.get('callback'):

            if message.text:
                models.ButtonQuestion.objects.create(button=self.user.profile.current_button, name=message.text,
                                                     type_content='text', type_question='normal')

            if message.photo:
                models.CommandQuestion.objects.create(command=self.user.profile.current_button, name='photo.jpg',
                                                      type_content='photo')
            self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get(
                'callback')
            self.user.profile.save()

            self.bot_setting(message.chat.id, message,
                             action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get('callback'))
        if self.user.profile.current_action == \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS.get('callback'):

            if message.text:
                models.ButtonQuestion.objects.create(button=self.user.profile.current_button, name=message.text,
                                                     type_content='text', type_question='full_dress')

            if message.photo:
                models.CommandQuestion.objects.create(command=self.user.profile.current_button, name='photo.jpg',
                                                      type_content='photo')

            self.user.profile.current_action = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get(
                    'callback')
            self.user.profile.current_action_edit = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get(
                'callback')
            self.user.profile.save()
            self.bot_setting(message.chat.id, message,
                        action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_FULL_DRESS_COUNT.get(
                                 'callback'))

        if self.user.profile.current_action == \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS.get('callback'):

            if message.text:
                models.ButtonQuestion.objects.create(button=self.user.profile.current_button, name=message.text,
                                                     type_content='text', type_question='with_variants_of_answers')

            if message.photo:
                models.CommandQuestion.objects.create(command=self.user.profile.current_button, name='photo.jpg',
                                                      type_content='photo')

            self.user.profile.current_action = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get(
                'callback')
            self.user.profile.current_action_edit = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get(
                'callback')
            self.user.profile.save()

            self.bot_setting(message.chat.id, message,
                             action_name =
                             actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_WITH_VARIANTS_ANSWERS_ACT.get(
                                 'callback'))
        if self.user.profile.current_action == \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE.get('callback'):

            if message.text:
                models.ButtonQuestion.objects.create(button=self.user.profile.current_button, name=message.text,
                                                     type_content='text', type_question='questionnaire')

            if message.photo:
                models.CommandQuestion.objects.create(command=self.user.profile.current_button, name='photo.jpg',
                                                      type_content='photo')

            self.user.profile.current_action = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get(
                'callback')
            self.user.profile.current_action_edit = \
                actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get(
                'callback')
            self.user.profile.save()

            self.bot_setting(message.chat.id, message,
                             action_name =
                             actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get(
                                 'callback'))

    def save_button(self, chat_id, message):
        self.user.profile.current_button.saved = True
        self.user.profile.current_button.save()
        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS.get('callback'))

    def add_question_to_button_count_messages(self, message):
        try:
            int(message.text)
            bq = models.ButtonQuestion.objects.get(id=self.user.profile.current_button.button_questions.all()[0].id)
            bq.count_message = message.text
            bq.save()
            self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get(
                'callback')
            self.user.profile.save()

            self.bot_setting(message.chat.id, message,
                             action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get('callback'))
        except:
            self.master_bot.send_message(message.chat.id, 'Нужно ввести целое число')

    def add_question_to_button_answers_variant(self, message):
        for answer in message.text.split('\n'):
            models.ButtonQuestionAnswer.objects.create(button_question_id=
                                                       self.user.profile.current_button.button_questions.filter(
                                                           type_question='with_variants_of_answers')[0].id, text=answer)
            self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get(
                'callback')
            self.user.profile.save()

            self.bot_setting(message.chat.id, message,
                             action_name=actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_SAVE.get('callback'))

    def add_question_to_button_questionnarie_next(self, message):
        models.ButtonQuestionNextQuestion.objects.create(button_question_id=
                                                         self.user.profile.current_button.button_questions.filter(
                                                             type_question='questionnaire')[0].id, name=message.text)
        self.bot_setting(message.chat.id, message,
                         action_name =
                         actions.BOT_SETTINGS_BUTTONS_CREATE_BUTTON_ACTION_ADD_QUESTION_QUESTIONNARIE_NEXT.get(
                             'callback'))

    def bot_settings_del_all_buttons(self, chat_id, message):
        for button in self.user.profile.bot_selected.buttons.all():
            button.delete()

        self.user.profile.current_action = actions.BOT_SETTINGS_BUTTONS.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_BUTTONS.get('callback'))

    def hello_start_over(self, chat_id, message):
        try:
            self.user.profile.bot_selected.first_welcome.delete()
        except:
            pass

        try:
            self.user.profile.bot_selected.two_welcome.delete()
        except:
            pass

        self.user.profile.current_action = actions.BOT_SETTINGS_HELLO.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_HELLO.get('callback'))

    def add_first_hello_text(self, message):
        try:
            self.user.profile.bot_selected.first_welcome
            first_welcome = models.FirstWelcomeBot.objects.get(id=self.user.profile.bot_selected.first_welcome.id)
            first_welcome.text = message.text
            first_welcome.save()
        except:
            models.FirstWelcomeBot.objects.create(bot=self.user.profile.bot_selected,
                                                  text=message.text)
        self.user.profile.current_action = actions.BOT_SETTINGS_HELLO.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_HELLO.get('callback'))

    def add_two_hello_text(self, message):
        try:
            self.user.profile.bot_selected.two_welcome
            first_welcome = models.TwoWelcomeBot.objects.get(id=self.user.profile.bot_selected.two_welcome.id)
            first_welcome.text = message.text
            first_welcome.save()
        except:
            models.TwoWelcomeBot.objects.create(bot=self.user.profile.bot_selected,
                                                text=message.text)
        self.user.profile.current_action = actions.BOT_SETTINGS_HELLO.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_HELLO.get('callback'))

    def add_first_hello_actions(self, chat_id, message):
        for button in self.user.profile.bot_selected.buttons.all():
            button.delete()

        self.user.profile.current_action = actions.BOT_SETTINGS_FIRST_HELLO.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_FIRST_HELLO.get('callback'))

    def add_first_hello_actions_save(self, message):
        first_hello_answer_types = {
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_BUTTONS.get('callback'): 'buttons',
            actions.BOT_SETTINGS_FIRST_HELLO_ADD_SMILES.get('callback'): 'smiles',
            actions.BOT_SETTINGS_FIRST_HELLO_DELAY.get('callback'): 'delay',
            actions.BOT_SETTINGS_FIRST_HELLO_MARKDOWN.get('callback'): 'markdown',
        }

        two_hello_answer_types = {
            actions.BOT_SETTINGS_TWO_HELLO_ADD_BUTTONS.get('callback'): 'buttons',
            actions.BOT_SETTINGS_TWO_HELLO_ADD_SMILES.get('callback'): 'smiles',
            actions.BOT_SETTINGS_TWO_HELLO_DELAY.get('callback'): 'delay',
            actions.BOT_SETTINGS_TWO_HELLO_MARKDOWN.get('callback'): 'markdown',
        }

        if first_hello_answer_types.get(self.user.profile.current_action):
            try:
                self.user.profile.bot_selected.first_welcome
                first_welcome = models.FirstWelcomeBot.objects.get(id=self.user.profile.bot_selected.first_welcome.id)
                first_welcome.text = message.text
                first_welcome.type_content = first_hello_answer_types.get(self.user.profile.current_action)
                first_welcome.save()
            except:
                models.FirstWelcomeBot.objects.create(bot=self.user.profile.bot_selected,
                                                      text=message.text,
                                                      type_content=first_hello_answer_types.get(
                                                          self.user.profile.current_action))

        if two_hello_answer_types.get(self.user.profile.current_action):
            try:
                self.user.profile.bot_selected.two_welcome
                first_welcome = models.TwoWelcomeBot.objects.get(id=self.user.profile.bot_selected.two_welcome.id)
                first_welcome.text = message.text
                first_welcome.type_content = two_hello_answer_types.get(self.user.profile.current_action)
                first_welcome.save()
            except:
                models.TwoWelcomeBot.objects.create(bot=self.user.profile.bot_selected,
                                                    text=message.text,
                                                    type_content=two_hello_answer_types.get(
                                                        self.user.profile.current_action))

        self.user.profile.current_action = actions.BOT_SETTINGS_HELLO.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_HELLO.get('callback'))

    def add_news_letter_text(self, message):
        try:
            self.user.profile.bot_selected.news_letter
            news_letter = models.NewsLetterBot.objects.get(id=self.user.profile.bot_selected.news_letter.id)
            news_letter.text = message.text
            news_letter.save()
        except:
            dt = datetime.now()
            models.NewsLetterBot.objects.create(bot=self.user.profile.bot_selected,
                                                text=message.text, departure_time='{}:{}'.format(dt.hour, dt.minute))
        self.user.profile.current_action = actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'))

    def add_news_letter_sent_time(self, message):
        try:
            self.user.profile.bot_selected.news_letter
            news_letter = models.NewsLetterBot.objects.get(id=self.user.profile.bot_selected.news_letter.id)
            news_letter.departure_time = message.text
            news_letter.save()
        except:
            pass

        self.user.profile.current_action = actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'))

    def add_news_letter_actions_save(self, message):
        news_letter_answer_types = {
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_BUTTONS.get('callback'): 'buttons',
            actions.BOT_SETTINGS_USERS_NEWS_LETTER_ADD_SMILES.get('callback'): 'smiles',
        }

        try:
            self.user.profile.bot_selected.news_letter
            news_letter = models.NewsLetterBot.objects.get(id=self.user.profile.bot_selected.news_letter.id)
            news_letter.text = message.text
            news_letter.type_content = news_letter_answer_types.get(self.user.profile.current_action)
            news_letter.save()
        except:
            dt = datetime.now()
            models.NewsLetterBot.objects.create(bot=self.user.profile.bot_selected,
                                                text=message.text,
                                                departure_time='{}:{}'.format(dt.hour, dt.minute),
                                                type_content=news_letter_answer_types.get(
                                                    self.user.profile.current_action))

        self.user.profile.current_action = actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback'))

    def news_letter_start_over(self, chat_id, message):
        try:
            self.user.profile.bot_selected.news_letter.delete()
        except:
            pass

        self.user.profile.current_action = actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback')
        self.user.profile.save
        keybord = self.bot_setting_keyboard(action=self.user.profile.current_action,
                                            bot_name=self.user.profile.bot_selected.name, chat_id=chat_id,
                                            message=message)
        self.master_bot.edit_message_text('Рассылка', chat_id=chat_id, message_id=self.message_id, reply_markup=keybord)

    def news_letter_is_regular(self, chat_id, message):
        try:
            self.user.profile.bot_selected.news_letter
            news_letter = models.NewsLetterBot.objects.get(id=self.user.profile.bot_selected.news_letter.id)
            news_letter.is_regular = True
            news_letter.save()
        except:
            pass

        self.user.profile.current_action = actions.BOT_SETTINGS_USERS_NEWS_LETTER.get('callback')
        self.user.profile.save
        keybord = self.bot_setting_keyboard(action=self.user.profile.current_action,
                                            bot_name=self.user.profile.bot_selected.name, chat_id=chat_id,
                                            message=message)
        self.master_bot.edit_message_text('Рассылка', chat_id=chat_id, message_id=self.message_id, reply_markup=keybord)

    def bot_chat_off(self, chat_id, message):
        bot = telebot.TeleBot(self.user.profile.bot_selected.token)
        for chat in self.user.profile.bot_selected.chats.all():
            try:
                bot.leave_chat(chat.name)
            except:
                print('ERROR')

        self.user.profile.current_action = actions.BOT_SETTINGS_MENU.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_MENU.get('callback'))

    def black_list_unblocked_all(self, chat_id, message):
        bot = telebot.TeleBot(self.user.profile.bot_selected.token)
        for black_list in self.user.profile.bot_selected.black_lists.all():
            black_list.delete()

        self.user.profile.current_action = actions.BOT_SETTINGS_MENU.get('callback')
        self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=actions.BOT_SETTINGS_MENU.get('callback'))

    def auto_posting_select_source(self, chat_id, message):
        print('auto_posting_select_source', self.user.profile.current_action)
        # self.user.profile.current_action = actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TIME.get('callback')
        # self.user.profile.save
        self.bot_setting(chat_id, message,
                         action_name=self.user.profile.current_action)

    def auto_posting_select_source_save(self, message):
        for publication in self.user.profile.bot_selected.auto_postings.filter(saved=False):
            publication.saved = True
            publication.save()

        auto_post_types = {
            actions.BOT_SETTINGS_AUTO_POSTING_CREATE_POST.get('callback'): 'my_post',
            actions.BOT_SETTINGS_AUTO_POSTING_TWITTER.get('callback'): 'twitter',
            actions.BOT_SETTINGS_AUTO_POSTING_VK.get('callback'): 'vk',
            actions.BOT_SETTINGS_AUTO_POSTING_YOUTUBE.get('callback'): 'youtube',
            actions.BOT_SETTINGS_AUTO_POSTING_FACEBOOK.get('callback'): 'facebook',
            actions.BOT_SETTINGS_AUTO_POSTING_INSTAGRAM.get('callback'): 'instagram',
            actions.BOT_SETTINGS_AUTO_POSTING_RSS.get('callback'): 'rss'
        }
        models.AutoPosting.objects.create(bot=self.user.profile.bot_selected,
                                          text=message.text,
                                          source=auto_post_types.get(self.user.profile.current_action))
        self.user.profile.current_action = actions.BOT_SETTINGS_AUTO_POSTING_CREATE_POST_SEND_DATA.get('callback')
        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=self.user.profile.current_action)

    def auto_posting_save_publication_times(self, message):
        public_times = ''
        for t in message.text.split('\n'):
            if re.search(r'[\d]{2,2}[:][\d]{2,2}', t) == None:
                self.master_bot.send_message(message.chat.id, 'Введено недопустимое значение')
                return False
            else:
                public_times = public_times + '/{}'.format(t)

        for publication in self.user.profile.bot_selected.auto_postings.filter(saved=False):
            publication.published_time = public_times
            publication.save()

        self.user.profile.current_action = actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY.get('callback')

        self.user.profile.save
        self.bot_setting(message.chat.id, message,
                         action_name=actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY.get('callback'))

    def auto_posting_save_selected_day(self, chat_id, message):
        action, param = message.data.split(':')

        current_post = models.AutoPosting.objects.get(bot=self.user.profile.bot_selected, saved=False)  #
        # current_post = self.user.profile.bot_selected.auto_postings.filter(saved=False)[0]

        print('###############################################1')
        print(current_post.everyday, 'everyday')
        print(current_post.in_one_day, 'in_one_day')
        print(current_post.monday, 'monday')
        print(current_post.tuesday, 'tuesday')
        print(current_post.wednesday, 'wednesday')
        print(current_post.thursday, 'thursday')
        print(current_post.friday, 'friday')
        print(current_post.saturday, 'saturday')
        print(current_post.sunday, 'sunday')
        print(current_post.published_off, 'published_off')
        print(current_post.id, '###############################################1')

        edit_fields = {
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_EVERY_DAY.get('callback'): 'everyday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_IN_ON_DAY.get('callback'): 'in_one_day',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_MONDAY.get('callback'): 'monday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TUESDAY.get('callback'): 'tuesday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_WEDNESDAY.get('callback'): 'wednesday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_THURSDAY.get('callback'): 'thursday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_FRIDAY.get('callback'): 'friday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SATURDAY.get('callback'): 'saturday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SUNDAY.get('callback'): 'sunday',
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_OFF.get('callback'): 'published_off'
        }

        # print(action, 'ACT', edit_fields.get(action), getattr(current_post, edit_fields.get(action)))
        if getattr(current_post, edit_fields.get(action)):
            setattr(current_post, edit_fields.get(action), False)
            current_post.save()
            print('NVF')
        else:
            current_post.save()
            print('NVT')
            setattr(current_post, edit_fields.get(action), True)

        current_post.save()
        # bn.update({action: getattr(current_post, action)})

        print(current_post.id, '###############################################2')
        print(current_post.everyday, 'everyday')
        print(current_post.in_one_day, 'in_one_day')
        print(current_post.monday, 'monday')
        print(current_post.tuesday, 'tuesday')
        print(current_post.wednesday, 'wednesday')
        print(current_post.thursday, 'thursday')
        print(current_post.friday, 'friday')
        print(current_post.saturday, 'saturday')
        print(current_post.sunday, 'sunday')
        print(current_post.published_off, 'published_off')
        print('###############################################2')

        bn = {
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_EVERY_DAY.get('callback'): current_post.everyday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_IN_ON_DAY.get('callback'): current_post.in_one_day,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_MONDAY.get('callback'): current_post.monday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_TUESDAY.get('callback'): current_post.tuesday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_WEDNESDAY.get('callback'): current_post.wednesday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_THURSDAY.get('callback'): current_post.thursday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_FRIDAY.get('callback'): current_post.friday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SATURDAY.get('callback'): current_post.saturday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SUNDAY.get('callback'): current_post.sunday,
            actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_OFF.get('callback'): current_post.published_off
        }

        keyboard = telebot.types.InlineKeyboardMarkup()

        for button_row in actions.keyboard_entities.get(
                actions.BOT_SETTINGS_AUTO_POSTING_PUBLISHED_SELECT_DAY.get('callback')):
            buttons = []
            for button in button_row:
                try:
                    # print(button.get('alternative_name').get('on_off').get(bn.get(button.get('callback'))))
                    buttons.append(telebot.types.InlineKeyboardButton(
                        text=button.get('alternative_name').get('on_off').get(bn.get(button.get('callback'))),
                        callback_data='{}:None'.format(button.get('callback'))))
                except:
                    buttons.append(telebot.types.InlineKeyboardButton(text=button.get('name'),
                                                                      callback_data='{}:None'.format(
                                                                          button.get('callback'))))

            keyboard.add(
                *[button for button in buttons])

        self.master_bot.delete_message(chat_id=chat_id, message_id=message.message.message_id)
        self.send_message(chat_id, 'Выбрать дни публикации', keyboard, message)
