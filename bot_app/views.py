from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings

import telebot

import json

from .reply_keyboard import RKeybord
from . import messages

from .bot_maker2 import BotMaker

from bot_app import models

settings.BOT.set_webhook(url=settings.WEBHOOK_URL_BASE + settings.WEBHOOK_URL_PATH,
                         certificate=open(settings.WEBHOOK_SSL_CERT, 'r'))


@csrf_exempt
def action2(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    update = telebot.types.Update.de_json(body)
    settings.BOT.process_new_updates([update])

    try:
        user = models.User.objects.get(username=body.get('message').get('chat').get('id'))
    except:
        pass

    try:
        user = models.User.objects.get(username=body.get('callback_query').get('message').get('chat').get('id'))
    except:
        pass

    try:
        uid = body.get('message').get('chat').get('id')
        user = models.User.objects.create_user(str(uid))
        models.ClientProfile.objects.create(user=user)
    except:
        pass

    try:
        print('UID2', body.get('callback_query').get('message').get('chat').get('id'))
        uid = body.get('callback_query').get('message').get('chat').get('id')
        user = models.User.objects.create_user(str(uid))
        models.ClientProfile.objects.create(user=user)
    except:
        pass

    print(user, "USER")

    bot_list = [
        {'name': bot.get('name'), 'callback_data': 'bot_setting:{}'.format(bot.get('name')), 'bot_object': bot}
        for bot in models.Bot.objects.filter(client=user).values()]

    # bot_maker = BotMaker(settings.BOT, user)
    reply_keyboard_obj = RKeybord()

    client_status = {}

    @settings.BOT.message_handler(commands=['start', 'help'])
    def send_welcome(message):
        msg = settings.BOT.send_message(message.chat.id, 'Конструктор ботов',
                                        reply_markup=reply_keyboard_obj.reply_keyboard)

    @settings.BOT.callback_query_handler(func=lambda message: True)
    def process_step(message):
        print(message.message.message_id, 'MID')
        action, param = message.data.split(':')
        user = models.User.objects.get(username=message.from_user.id)

        user.profile.current_action = action
        user.profile.save()

        print(action, 'CA')
        bot_maker = BotMaker(settings.BOT, user, message.message.message_id)
        bot_maker.get_work_method(action)(message.from_user.id, message)

        if action in bot_maker.edit_methods.keys():
            user.profile.current_action_edit = action
            user.profile.save()
            print('EDIT METHOD')

    @settings.BOT.message_handler(func=lambda message: True, content_types=['text'])
    def add_answer(message):
        print(message.message_id, 'MIB')
        user = models.User.objects.get(username=message.chat.id)
        bot_maker = BotMaker(settings.BOT, user, message.message_id)
        print(user.profile.current_action_edit, 'FFFFFFFF')
        text_message = message.text
        msg = settings.BOT.send_message(message.chat.id, text_message,
                                        reply_markup=reply_keyboard_obj.reply_keyboard)

        if message.text == 'Мои боты':
            user.profile.bot_selected = None
            user.profile.save()
            if client_status.get(message.from_user.id):
                client_status.pop(message.from_user.id)
            bot_maker.my_bots(message.chat.id, message)
            settings.BOT.register_next_step_handler(msg, add_answer)

        if message.text == 'Как это работает?':
            user.profile.bot_selected = None
            user.profile.save()
            if client_status.get(message.from_user.id):
                client_status.pop(message.from_user.id)
            settings.BOT.send_message(message.chat.id, text=messages.how,
                                      parse_mode='Markdown')
            settings.BOT.register_next_step_handler(msg, add_answer)

        if message.text == 'Поддержка':
            user.profile.bot_selected = None
            user.profile.save()
            if client_status.get(message.from_user.id):
                client_status.pop(message.from_user.id)
            settings.BOT.send_message(message.chat.id, text=messages.support,
                                      parse_mode='Markdown')
            settings.BOT.register_next_step_handler(msg, add_answer)

        if message.text == 'Добавить бота':
            user.profile.bot_selected = None
            user.profile.save()
            client_status[message.from_user.id] = {'token': 'wait'}
            markup = telebot.types.InlineKeyboardMarkup()
            cancel_button = telebot.types.InlineKeyboardButton(text='Отмена', callback_data='AddBotCancel')
            markup.add(cancel_button)
            settings.BOT.send_message(message.chat.id,
                                      text='Отправьте токен, или перешлите сообщение с токеном от @BotFather.',
                                      reply_markup=markup)
            settings.BOT.register_next_step_handler(msg, add_answer)

        if client_status.get(message.from_user.id):
            if client_status.get(message.from_user.id).get('token') == 'wait' and message.text != 'Добавить бота':
                print('NB', message.text)
                new_bot = telebot.TeleBot(message.text)
                res = new_bot.get_me()
                try:
                    new_bot = telebot.TeleBot(message.text)
                    res = new_bot.get_me()
                    if res.is_bot == True and res.username not in bot_list:
                        client_status.pop(message.from_user.id)
                        client = models.User.objects.get(username=message.from_user.id)

                        if models.Bot.objects.filter(client=client, name=res.username).count() > 0:
                            text_message = 'Этот бот уже есть в списке. Перейти к настройкам?'
                        else:
                            new_bot = models.Bot.objects.create(client=client, name=res.username, token=message.text)
                            text_message = 'Бот успешно добавлен.'

                        client.profile.bot_selected = new_bot
                        client.profile.save()
                        markup = telebot.types.InlineKeyboardMarkup()
                        cancel_button = telebot.types.InlineKeyboardButton(text='Перейти к настройкам бота',
                                                                           callback_data='botSetting:{}'.format(
                                                                               new_bot.name))
                        markup.add(cancel_button)
                        settings.BOT.send_message(message.chat.id,
                                                  text=text_message,
                                                  reply_markup=markup)
                        settings.BOT.register_next_step_handler(msg, add_answer)
                except:
                    msg = settings.BOT.send_message(message.chat.id, 'Не корректный токен',
                                                    reply_markup=reply_keyboard_obj.reply_keyboard)
                    settings.BOT.register_next_step_handler(msg, add_answer)

        # Обработка входящих данных
        # try:
        print(user, user.profile.current_action_edit, 'EDIT')
        bot_maker.edit_methods.get(user.profile.current_action_edit)(message)
        # except:
        #    pass

    return JsonResponse({})
