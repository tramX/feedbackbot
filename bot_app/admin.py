from django.contrib import admin

from bot_app import models

admin.site.register(models.Blocked)
admin.site.register(models.Bot)
admin.site.register(models.CommandBot)
admin.site.register(models.BotUser)
admin.site.register(models.Chat)
admin.site.register(models.CommandQuestion)
admin.site.register(models.ClientProfile)
admin.site.register(models.ButtonBot)
admin.site.register(models.ButtonQuestion)
admin.site.register(models.ButtonQuestionAnswer)
admin.site.register(models.ButtonQuestionNextQuestion)
admin.site.register(models.FirstWelcomeBot)
admin.site.register(models.TwoWelcomeBot)
admin.site.register(models.NewsLetterBot)
admin.site.register(models.BlackList)
admin.site.register(models.AutoPosting)
