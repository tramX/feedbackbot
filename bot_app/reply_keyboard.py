import telebot

class RKeybord:
    def __init__(self):
        # Нижняя клавиатура
        self.reply_keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
        self.reply_keyboard.add(*[telebot.types.KeyboardButton('Добавить бота'),
                                  telebot.types.KeyboardButton('Мои боты')])
        self.reply_keyboard.add(*[telebot.types.KeyboardButton('Как это работает?'),
                                  telebot.types.KeyboardButton('Поддержка')])