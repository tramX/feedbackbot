from django.db import models

from django.contrib.auth.models import User


class Bot(models.Model):
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='bots')
    name = models.CharField(max_length=255, verbose_name='Название бота')
    token = models.TextField(verbose_name='Токен бота')

    class Meta:
        verbose_name = "Бот"
        verbose_name_plural = "Боты"

    def __str__(self):
        return '{} {} {}'.format(self.client.username, self.name, self.token)


class CommandBot(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='commands')
    name = models.CharField(max_length=255, verbose_name='Название команды')
    random_message = models.BooleanField(default=False, verbose_name='Возвращать случайное сообщение')
    saved = models.BooleanField(default=False, verbose_name='Комманда сохранена')

    class Meta:
        verbose_name = "Команда"
        verbose_name_plural = "Команды"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.name, self.saved)


class Blocked(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='blockeds')
    name = models.CharField(max_length=255, verbose_name='Название клиента')

    class Meta:
        verbose_name = "Заблокировал"
        verbose_name_plural = "Заблокировали"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.name)


class BotUser(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='users')
    name = models.CharField(max_length=255, verbose_name='Название клиента')

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.name)


class Chat(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='chats')
    name = models.CharField(max_length=255, verbose_name='Название чата')

    class Meta:
        verbose_name = "Чат"
        verbose_name_plural = "Чаты"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.name)


class CommandQuestion(models.Model):
    TYPE_CHOICES = (
        ('text', 'Текст'),
        ('photo', 'Фото'),
    )
    command = models.ForeignKey(CommandBot, on_delete=models.CASCADE, related_name='command_questions')
    name = models.CharField(max_length=255, verbose_name='Название вопроса')
    type_content = models.CharField(max_length=255, verbose_name='Тип контента', choices=TYPE_CHOICES)

    class Meta:
        verbose_name = "Вопрос для команды"
        verbose_name_plural = "Вопросы для команды"

    def __str__(self):
        return '{} {} {}'.format(self.command.name, self.name, self.type_content)


class ButtonBot(models.Model):
    IN_DOWN_CHAT = (
        ('in_chat', 'В чате'),
        ('down_chat', 'Под чатом'),
    )
    MESSAGE_MULTI_MENU = (
        ('new_message', 'Новое сообщение'),
        ('multi_menu', 'Мультименю'),
    )

    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='buttons')
    name = models.CharField(max_length=255, verbose_name='Название кнопки')
    random_message = models.BooleanField(default=False, verbose_name='Возвращать случайное сообщение')
    in_down_chat = models.CharField(max_length=255, choices=IN_DOWN_CHAT, default='in_chat')
    message_multi_menu = models.CharField(max_length=255, choices=MESSAGE_MULTI_MENU, default='new_message')
    password = models.CharField(max_length=255, verbose_name='Пароль', blank=True, null=True)
    saved = models.BooleanField(default=False, verbose_name='Кнопка сохранена')

    class Meta:
        verbose_name = "Кнопка"
        verbose_name_plural = "Кнопки"

    def __str__(self):
        return '{} {} {} {} {}'.format(self.bot.name, self.name, self.random_message,
                                       self.password, self.saved)


class ClientProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    current_action = models.CharField(max_length=255, default='')
    current_action_edit = models.CharField(max_length=255, default='')
    bot_selected = models.ForeignKey(Bot, blank=True, null=True, on_delete=models.SET_NULL)
    end_item_number = models.PositiveIntegerField(default=0)
    current_command = models.ForeignKey(CommandBot, blank=True, null=True, on_delete=models.SET_NULL)
    current_button = models.ForeignKey(ButtonBot, blank=True, null=True, on_delete=models.SET_NULL)
    create_button_in_down_chat = models.CharField(max_length=255, default='in_chat')
    message_multi_menu = models.CharField(max_length=255, default='new_message')

    class Meta:
        verbose_name = "Профиль клиента"
        verbose_name_plural = "Профили клиентов"

    def __str__(self):
        return '{} {} {} {}'.format(self.user.username, self.current_action,
                                    self.create_button_in_down_chat, self.message_multi_menu)


class ButtonQuestion(models.Model):
    TYPE_CONTENT_CHOICES = (
        ('text', 'Текст'),
        ('photo', 'Фото'),
    )

    TYPE_QUESTION_CHOICES = (
        ('normal', 'Обычный'),
        ('full_dress', 'Развернутый'),
        ('with_variants_of_answers', 'С вариантами ответов'),
        ('questionnaire', 'Анкета'),
    )

    button = models.ForeignKey(ButtonBot, on_delete=models.CASCADE, related_name='button_questions')
    name = models.CharField(max_length=255, verbose_name='Название вопроса')
    type_content = models.CharField(max_length=255, verbose_name='Тип контента', choices=TYPE_CONTENT_CHOICES)
    type_question = models.CharField(max_length=255, verbose_name='Тип вопроса', choices=TYPE_QUESTION_CHOICES)
    count_message = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = "Вопрос для кнопки"
        verbose_name_plural = "Вопросы для кнопок"

    def __str__(self):
        return '{} {} {} {} {}'.format(self.button.name, self.name, self.type_content, self.type_question,
                                       self.count_message)


class ButtonQuestionAnswer(models.Model):
    button_question = models.ForeignKey(ButtonQuestion, on_delete=models.CASCADE,
                                        related_name='button_question_answers')
    text = models.CharField(max_length=255, verbose_name='Ответ')

    class Meta:
        verbose_name = "Вариант ответа"
        verbose_name_plural = "Варианты ответов"

    def __str__(self):
        return '{} {}'.format(self.button_question.name, self.text)


class ButtonQuestionNextQuestion(models.Model):
    button_question = models.ForeignKey(ButtonQuestion, on_delete=models.CASCADE,
                                        related_name='button_question_next_questions')
    name = models.CharField(max_length=255, verbose_name='Название вопроса')

    class Meta:
        verbose_name = "Анкета следующий вопрос"
        verbose_name_plural = "Анкета следующие вопросы"

    def __str__(self):
        return '{} {}'.format(self.button_question.name, self.name)


class FirstWelcomeBot(models.Model):
    TYPE_CONTENT_CHOICES = (
        ('text', 'Текст'),
        ('buttons', 'Кнопки'),
        ('smiles', 'Смайлики'),
        ('delay', 'Задержка'),
        ('markdown', 'markdown'),
    )

    bot = models.OneToOneField(Bot, on_delete=models.CASCADE, related_name='first_welcome')
    text = models.TextField(verbose_name='Первое приветствие')
    type_content = models.CharField(max_length=255, verbose_name='Тип контента', choices=TYPE_CONTENT_CHOICES,
                                    default='text')

    class Meta:
        verbose_name = "Первое приветствие"
        verbose_name_plural = "Первые приветствия"

    def __str__(self):
        return '{} {} {}'.format(self.bot.name, self.text, self.type_content)


class TwoWelcomeBot(models.Model):
    TYPE_CONTENT_CHOICES = (
        ('text', 'Текст'),
        ('buttons', 'Кнопки'),
        ('smiles', 'Смайлики'),
        ('delay', 'Задержка'),
        ('markdown', 'markdown'),
    )

    bot = models.OneToOneField(Bot, on_delete=models.CASCADE, related_name='two_welcome')
    text = models.TextField(verbose_name='Второе приветствие')
    type_content = models.CharField(max_length=255, verbose_name='Тип контента', choices=TYPE_CONTENT_CHOICES,
                                    default='text')

    class Meta:
        verbose_name = "Второе приветствие"
        verbose_name_plural = "Вторые приветствия"

    def __str__(self):
        return '{} {} {}'.format(self.bot.name, self.text, self.type_content)


class NewsLetterBot(models.Model):
    TYPE_CONTENT_CHOICES = (
        ('text', 'Текст'),
        ('buttons', 'Кнопки'),
        ('smiles', 'Смайлики'),
    )

    bot = models.OneToOneField(Bot, on_delete=models.CASCADE, related_name='news_letter')
    text = models.TextField(verbose_name='Содержимое рассылки')
    type_content = models.CharField(max_length=255, verbose_name='Тип контента', choices=TYPE_CONTENT_CHOICES,
                                    default='text')
    departure_time = models.TimeField(verbose_name='Время отправления')
    is_regular = models.BooleanField(default=False, verbose_name='Регулярная рассылка')

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return '{} {} {}'.format(self.bot.name, self.text, self.type_content)


class BlackList(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='black_lists')
    name = models.CharField(max_length=255, verbose_name='ID клиента')

    class Meta:
        verbose_name = "В черном списке"
        verbose_name_plural = "В черных списках"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.name)


class AutoPosting(models.Model):
    SOURCE_CHOICES = (
        ('my_post', 'Мой пост'),
        ('twitter', 'twitter'),
        ('vk', 'vk'),
        ('youtube', 'youtube'),
        ('facebook', 'facebook'),
        ('instagram', 'instagram'),
        ('rss', 'rss'),
    )

    bot = models.ForeignKey(Bot, on_delete=models.CASCADE, related_name='auto_postings')
    text = models.TextField(max_length=255, verbose_name='Текст', blank=True, null=True)
    everyday = models.BooleanField(default=True, verbose_name='Каждый день')
    in_one_day = models.BooleanField(default=True, verbose_name='Через день')
    monday = models.BooleanField(default=True, verbose_name='Понедельник')
    tuesday = models.BooleanField(default=True, verbose_name='Вторник')
    wednesday = models.BooleanField(default=True, verbose_name='Среда')
    thursday = models.BooleanField(default=True, verbose_name='Четверг')
    friday = models.BooleanField(default=True, verbose_name='Пятница')
    saturday = models.BooleanField(default=True, verbose_name='Суббота')
    sunday = models.BooleanField(default=True, verbose_name='Воскресенье')
    source = models.CharField(max_length=255, verbose_name='Тип поста', choices=SOURCE_CHOICES,
                              default='my_post')
    published_time = models.CharField(max_length=255, verbose_name='время публикации', blank=True, null=True)
    saved = models.BooleanField(default=False)
    published_off = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Авто постинг"
        verbose_name_plural = "Автопостинги"

    def __str__(self):
        return '{} {}'.format(self.bot.name, self.text)
